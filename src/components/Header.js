import "./Header.css"
import SearchIcon from '@mui/icons-material/Search';
import MenuIcon from '@mui/icons-material/Menu';
import LogoDevIcon from '@mui/icons-material/LogoDev';
import React from 'react'
import Search from './Search';

function Header() {
  
  return (
    <div className="header">

      <div className="header-left">
        <LogoDevIcon className="logo-icon" />
      </div>

      <div className="header-center">
        <div className="header-search"></div>
        <SearchIcon />

        <Search />

      </div>

      <div className="header-right">
        <MenuIcon className="menu-icon" />
      </div>
      
    </div>
  )
}

export default Header
